/* vim: set et ts=4 tw=92:
 * Copyright (C) 2017-2018  Jonathan Lebon <jonathan@jlebon.com>
 * This file is part of Textern.
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

"use strict";

function assertNoResponse(response) {
    console.assert(response == undefined, `No response to message expected. Received one anyway:\n${response}`);
}

function notifyError(error) {
    messenger.notifications.create({
        type: "basic",
        title: "Textern",
        message: "Error: " + error + "."
    });
}

function registerText(event) {
    const e = event.target;

    // This is used to generate a unique filename in firefox version of this extension.
    // NOTE: Only kept alive, s.t. the same Python script can be used.
    const simple_url = 'textern-tb';

    // TODO: Check if there's a possibility to get current caret position
    e.selectionStart = 0;

    messenger.runtime.sendMessage({
        type: "register_text",
        caret: e.selectionStart,
        url: simple_url
    }).then(assertNoResponse, console.log);
}

var currentShortcut = undefined;
function registerShortcut(force) {
    messenger.storage.local.get({shortcut: "Ctrl+Shift+D"}).then(val => {
        if ((val.shortcut == currentShortcut) && !force)
            return; /* no change */
        if (currentShortcut != undefined)
            shortcut.remove(currentShortcut);
        currentShortcut = val.shortcut;
        shortcut.add(currentShortcut, registerText);
    });
}

registerShortcut(true);

/* meh, we just re-apply the shortcut -- XXX: should check what actually changed */
messenger.storage.onChanged.addListener(function(changes, areaName) {
    registerShortcut(false);
});

