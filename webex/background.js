/* vim: set et ts=4 tw=92:
 * Copyright (C) 2017-2018  Jonathan Lebon <jonathan@jlebon.com>
 * This file is part of Textern.
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

"use strict";

var port = undefined;

var activeDocs = [];

function notifyError(error) {
    messenger.notifications.create({
        type: "basic",
        title: "Textern",
        message: "Error: " + error + "."
    });
}

function contentSetActiveText(tid, text) {
    messenger.compose.setComposeDetails(tid, { plainTextBody: text })
}

function handleNativeMessage(msg) {
    if (msg.type == "text_update") {
        const tid = parseInt(msg.payload.id.split("_")[0]);
        if (tid == NaN) {
            console.log(`Invalid id: ${id}`);
            return;
        }
        contentSetActiveText(tid, msg.payload.text);
    } else if (msg.type == "death_notice") {
        unregisterDoc(msg.payload.id);
    } else if (msg.type == "error") {
        notifyError(msg.payload.error);
    } else {
        console.log(`Unknown native message type: ${msg.type}`);
    }
}

function unregisterDoc(id) {
    const i = activeDocs.indexOf(id);
    if (i == -1) {
        console.log(`Error: document id ${id} isn't being edited`);
        return;
    }

    activeDocs.splice(i, 1);
    if (activeDocs.length == 0) {
        port.disconnect();
        port = undefined;
    }
}

function registerDoc(tid, text, caret, url) {
    const id = `${tid}`;
    if (activeDocs.indexOf(id) != -1) {
        console.log(`Error: document id ${id} is already being edited`);
        notifyError("this text is already being edited");
        return;
    }

    activeDocs.push(id);
    if (port == undefined) {
        port = messenger.runtime.connectNative("textern.thunderbird");
        port.onMessage.addListener((response) => {
            handleNativeMessage(response);
        });
        port.onDisconnect.addListener((p) => {
            console.log("Disconnected from helper");
            if (p.error) {
                console.error(p.error);
            }
            activeDocs = [];
            port = undefined;
        });
    }

    messenger.storage.local.get({
        editor: "[\"gedit\", \"+%l:%c\"]",
    }).then(values => {
        port.postMessage({
            type: "new_text",
            payload: {
                id: id,
                text: text,
                caret: caret,
                url: url,
                prefs: {
                    editor: values.editor,
                    extension: "eml",
                }
            }
        });
    }, console.error);
}

function handleRegisterText(tabId, message) {
    registerDoc(tabId, message.text, message.caret, message.url);
}

function onMessage(message, sender, respond) {
    if (message.type == "register_text") {
        const tabId = sender.tab.id;
        const composeDetails = messenger.compose.getComposeDetails(tabId).then(composeDetails => {
            message.text = composeDetails.plainTextBody;
            handleRegisterText(tabId, message);
        });
    } else {
        console.log(`Unknown message type: ${message.type}`);
    }
}

messenger.runtime.onMessage.addListener(onMessage);

messenger.composeScripts.register({
    js: [
        { file: "shortcut.js" },
        { file: "content.js" }
    ]
});

